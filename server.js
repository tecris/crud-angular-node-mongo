var port = 3000;
var express = require('express');
var app = express();
var mongoose = require('mongoose'); // mongoose for mongodb
var common = require('./config/common')
var config = common.config();

mongoose.connect(config.mongodbUrl, function(err, res) {
  if (err) {
    console.log('Error connecting to the database. ' + err);
  } else {
    console.log('Connected to Database: ' + config.mongodbUrl);
  }
});
var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

var path = require('path');

var movieApi = require('./server/routes/api/v1/movie');

// static setup
app.use(express.static(__dirname + '/app'));

app.use('/api/v1/movies', movieApi);

app.listen(port, function() {
  console.log('Now serving http://localhost:' + port + '/index.html');
});
