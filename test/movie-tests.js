var supertest = require('supertest');
var api = supertest('http://localhost:3000');
var url = "http://localhost:3000";
var chai = require('chai');
var expect = chai.expect;
var chaiHttp = require('chai-http');
var should = chai.should();

chai.use(chaiHttp);

var id;

describe('Movie testing', function() {
  it('Create movie', createMovie);
});

describe('Movie testing', function() {
  it('Should find the movie', checkMovieExists);
});

describe('Movie testing', function() {
  it('Update movie', updateMovie);
});

describe('Movie testing', function() {
  it('Delete movie', deleteMovie);
});

describe('Movie testing', function() {
  it('Invalid URL - should return a 404 response', function(done) {
    api.get('/api/vz/movies')
      .set('Accept', 'application/json')
      .expect(404, done);
  });
});

function createMovie(done) {

  var movie = {
    title: "Cloud Atlas",
    director: {
      firstName: "The",
      lastName: "Wachowskis"
    },
    country: "Germany",
    year: "2012"
  };

  api.post('/api/v1/movies')
    .set('Accept', 'json')
    .send(movie)
    .end(function(err, res) {
      expect(res.status).to.equal(200);
      expect(res.body).to.have.property("_id");
      id = res.body._id;
      expect(res.body).to.have.property("title");
      expect(res.body.title).to.equal("Cloud Atlas");
      expect(res.body).to.have.property("director");
      expect(res.body.director).to.have.property("firstName");
      expect(res.body.director.firstName).to.equal("The");
      expect(res.body.director).to.have.property("lastName");
      expect(res.body.director.lastName).to.equal("Wachowskis");
      expect(res.body).to.have.property("country");
      expect(res.body.country).to.equal("Germany");
      expect(res.body).to.have.property("year");
      expect(res.body.year).to.equal(2012);
      done();
    });
};


function checkMovieExists(done) {

  api.get('/api/v1/movies/' + id)
    .set('Accept', 'application/json')
    .end(function(err, res) {
      expect(res.status).to.equal(200);
      expect(res.body).to.have.property("_id");
      expect(res.body).to.have.property("title");
      expect(res.body.title).to.equal("Cloud Atlas");
      expect(res.body).to.have.property("director");
      expect(res.body.director).to.have.property("firstName");
      expect(res.body.director.firstName).to.equal("The");
      expect(res.body.director).to.have.property("lastName");
      expect(res.body.director.lastName).to.equal("Wachowskis");
      expect(res.body).to.have.property("country");
      expect(res.body.country).to.equal("Germany");
      expect(res.body).to.have.property("year");
      expect(res.body.year).to.equal(2012);
      done();
    });
}


function updateMovie(done) {

  var movie = {
    title: "Cloud Atlas",
    director: {
      firstName: "The",
      lastName: "Wachowskis"
    },
    country: "USA",
    year: "2012"
  };

  api.put('/api/v1/movies/' + id)
    .set('Accept', 'json')
    .send(movie)
    .end(function(err, res) {
      expect(res.status).to.equal(200);
      expect(res.body).to.have.property("_id");
      expect(res.body._id).to.equal(id);
      expect(res.body).to.have.property("title");
      expect(res.body.title).to.equal("Cloud Atlas");
      expect(res.body).to.have.property("director");
      expect(res.body.director).to.have.property("firstName");
      expect(res.body.director.firstName).to.equal("The");
      expect(res.body.director).to.have.property("lastName");
      expect(res.body.director.lastName).to.equal("Wachowskis");
      expect(res.body).to.have.property("country");
      expect(res.body.country).to.equal("USA");
      expect(res.body).to.have.property("year");
      expect(res.body.year).to.equal(2012);
      done();
    });
};


function deleteMovie(done) {
  api.delete('/api/v1/movies/' + id)
    .end(function(error, response) {
      response.should.have.status(200);
      done();
    });
}
