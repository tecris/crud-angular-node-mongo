#Playground for AngularJS, NodeJS, etc

[![Dependency Status](https://david-dm.org/tecris/crud-angular-node-mongo.svg)](https://david-dm.org/tecris/crud-angular-node-mongo)
[![Circle CI](https://circleci.com/gh/tecris/crud-angular-node-mongo.svg?style=svg)](https://circleci.com/gh/tecris/crud-angular-node-mongo)
[![Build Status](https://travis-ci.org/tecris/crud-angular-node-mongo.svg?branch=master)](https://travis-ci.org/tecris/crud-angular-node-mongo)

 - [**Development**](#development)
 - [**API**](#rest-api)
 - [**Kubernetes**](#kubernetes)

## Development
```
$ docker run --name movie-mongodb -d -p 27017:27017 mongo:3.2    # start mongo instance
$ npm install                            # install dependencies
$ npm start                              # start node
$ node app.js                            # start node, alternative
$ nodemon app.js                         # start node, suitable for dev
$ npm install supertest chai chai-http   # install test dependencies
$ npm test                               # run tests
$ mocha test                             # run tests, alternative
```

## REST API
 - Examples
  - **Create**

    ```
      $ curl -i -H "Content-Type: application/json" -H "Accept: application/json" -X POST -d '{
        "title": "Cloud Atlas",
        "director": {
          "firstName": "The",
          "lastName": "Wachowskis"
        },
        "country": "Germany",
        "year": "2012"
      }' http://localhost:3000/api/v1/movies
    ```
  - **Read (get all)**

    `$ curl -i -H "Accept: application/json" http://localhost:3000/api/v1/movies`

  - **Read (get one)**

    `$ curl -i -H "Accept: application/json" http://localhost:3000/api/v1/movies/:movieId`
  - **Update**

    ```
      $ curl -i -H "Content-Type: application/json" -H "Accept: application/json" -X PUT -d '{
          "_id": "56c6eafe2a75f2156b3d31dc",
          "title": "Cloud Atlas",
          "director": {
            "firstName": "The",
            "lastName": "Wachowskis"
          },
          "country": "Germany-USA",
          "year": "2012"
        }' http://localhost:3000/api/v1/movies/56c6eafe2a75f2156b3d31dc
    ```

  - **Delete**

    `$ curl -i -X DELETE http://localhost:3000/api/v1/movies/56c6eafe2a75f2156b3d31dc`

## Kubernetes

 * Deploy

   ```$ ./deployToKubernetes.sh```
   
 * Rolling update
 
   ```
   # Replication Controller rolling update from "danube-web-rc-v2" to "danube-web-rc-v3".
   # (image change from bluesky/danube:2 to bluesky/danube:3)
   $ kubectl rolling-update danube-web-rc-v2 danube-web-rc-v3 -f danube-web-rc-v3.yaml
   ```
   `http://192.168.122.51:30003`
