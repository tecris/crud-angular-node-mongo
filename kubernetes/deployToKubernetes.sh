#!/bin/bash

kubectl create -f danube-db-pod.yaml
kubectl create -f danube-db-svc.yaml
kubectl create -f danube-web-rc-v2.yaml
kubectl create -f danube-web-svc.yaml
