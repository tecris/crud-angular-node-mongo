################################################################################
#
# .) Build
# $ docker build --no-cache -t bluesky/danube:1.0.3 .
#
################################################################################
FROM mhart/alpine-node:5.6.0

WORKDIR /src
ADD . .

RUN npm install

EXPOSE 3000
CMD ["npm", "start"]
