angular.module('movieDemo').factory('MovieResource', function($resource){
    var resource = $resource('api/v1/movies/:MovieId',{MovieId:'@_id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:true},'update':{method:'PUT'}});
    return resource;
});
