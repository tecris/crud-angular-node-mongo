angular.module('movieDemo').controller('EditMovieController', function($scope, $routeParams, $location, MovieResource) {

  $scope.get = function() {
    var successCallback = function(data) {
      self.original = data;

      $scope.movie = new MovieResource(self.original);
    };
    var errorCallback = function() {
      $location.path("/movie");
    };
    MovieResource.get({
      MovieId: $routeParams.MovieId
    }, successCallback, errorCallback);
  };

  $scope.isClean = function() {
    return angular.equals(self.original, $scope.movie);
  };

  $scope.save = function() {
    var successCallback = function() {
      $scope.get();
    };
    $scope.movie.$update(successCallback);
  };

  $scope.cancel = function() {
    $location.path("/movie");
  };

  $scope.remove = function() {
      var successCallback = function() {
          $location.path("/movie");
      };
      $scope.movie.$remove(successCallback);
  };

  $scope.get();
});
