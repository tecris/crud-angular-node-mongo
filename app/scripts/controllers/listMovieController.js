angular.module('movieDemo').controller('ListMovieController', function($scope, MovieResource) {

  $scope.movies = MovieResource.queryAll();
});
