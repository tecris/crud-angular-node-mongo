angular.module('movieDemo').controller('NewMovieController', function($scope, $location, MovieResource) {

  $scope.$location = $location;
  $scope.movie = $scope.movie || {};

  $scope.save = function() {
    var successCallback = function(data, responseHeaders) {
      $location.path('/movie');
    };
    MovieResource.save($scope.movie, successCallback);
  };

  $scope.cancel = function() {
    $location.path("/movie");
  };
});
