var app = angular.module('movieDemo', ['ngRoute', 'ngResource']);

app.config(['$routeProvider', function($routeProvider) {
  $routeProvider

  // route for the home page
    .when('/', {
    templateUrl: 'views/landing.html',
    controller: 'mainController'
  })

  .when('/movie/edit/:MovieId', {
    templateUrl: 'views/movie/edit.html',
    controller: 'EditMovieController'
  })

  .when('/movie/new', {
      templateUrl: 'views/movie/edit.html',
      controller: 'NewMovieController'
    })
    // route for the movie details page
    .when('/movie', {
      templateUrl: 'views/movie/detail.html',
      controller: 'ListMovieController'
    });
}]);

app.controller('mainController', function($scope) {
  // create a message to display in our view
  $scope.message = 'Movie Casa ';
});

app.controller('NavController', function($scope, $location) {
  $scope.matchesRoute = function(route) {
    var path = $location.path();
    return (path === ("/" + route) || path.indexOf("/" + route + "/") == 0);
  }
});
