var Movie = require('../../../model/movie');
var express = require('express');
var router = express.Router();

router.get('/', findAllMovies);
router.get('/:id', findMovieById);
router.post('/', createMovie);
router.put('/:id', updateMovie);
router.delete('/:id', deleteMovie);

function findAllMovies(req, res) {
  Movie.find({}, function(err, movies) {
    // if there is an error retrieving, send the error. nothing after res.send(err) will execute
    if (err) {
      res.send(err);
    }
    res.json(movies); // return all movies in JSON format
  });
};

function findMovieById(req, res) {
  Movie.findById(req.params.id, function(err, movie) {
    if (err) {
      res.json({
        'ERROR': err
      });
    } else {
      res.json(movie);
    }
  });
};

function createMovie(req, res) {
  var newMovie = new Movie(req.body);
  // call the built-in save method to save to the database
  newMovie.save(function(err) {
    if (err) {
      res.json({
        'ERROR': err
      });
    } else {
      res.json(newMovie);
    }
  });
};

function updateMovie(req, res) {
  Movie.findById(req.params.id, function(err, movie) {
      movie.title = req.body.title,
      movie.director.firstName = req.body.director.firstName,
      movie.director.lastName = req.body.director.lastName,
      movie.country = req.body.country,
      movie.year = req.body.year
    movie.save(function(err) {
      if (err) {
        res.json({
          'ERROR': err
        });
      } else {
        res.json(movie);
      }
    });
  });
};

// *** delete SINGLE movie *** //
function deleteMovie(req, res) {
  Movie.findById(req.params.id, function(err, movie) {
    if(err) {
      res.json({'ERROR': err});
    } else {
      movie.remove(function(err){
        if(err) {
          res.json({'ERROR': err});
        } else {
          res.json({'REMOVED': movie});
        }
      });
    }
  });
}

module.exports = router;
