var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var movieSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  director:{
    firstName: String,
    lastName: String
  },
  country: String,
  year: Number
});

// the schema is useless so far
// we need to create a model using it
var Movie = mongoose.model('Movie', movieSchema);

// make this available to our users in our Node applications
module.exports = Movie;
